const mongoose = require("mongoose")

const schema = mongoose.Schema({
	judul: String,
	isi: String,
	url_img: String
})

module.exports = mongoose.model("Blog", schema)